### Retrieve Issue ###
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} ${JIRA_BASE_URL}/rest/api/latest/issue/BOOT-1

### Update Issue ###
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} PUT ${JIRA_BASE_URL}/rest/api/latest/issue/BOOT-1 fields:='
{
  "summary": "[BOOTCAMP] REST Ye Merry Gentlemen."
}'

### Delete Issue ####
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} DELETE ${JIRA_BASE_URL}/rest/api/latest/issue/BOOT-1