#!/usr/bin/env bats

@test "Git MUST be available." {
  run which git
  [ "$status" -eq 0 ]
}

@test "Git MUST run." {
  run git --version
  [ "$status" -eq 0 ]
  [ -n "${output}" ]
  # Check the first line,
  # if we start to care about version.
  # [ "${lines[0]}" = "" ]
}

@test "HTTPie MUST be available." {
  run which http
  [ "$status" -eq 0 ]
}

@test "HTTPie MUST run." {
  run http --version
  # Old versions of HTTPie still installed by apt-get,
  # return 1 from --version
  # [ "$status" -eq 0 ]
  [ -n "${output}" ]
  # Check the first line,
  # if we start to care about version.
  # [ "${lines[0]}" = "" ]
}

@test "JIRA environment variables MUST be available." {
  [ -n "${JIRA_SITENAME}" ]
  [ -n "${JIRA_BASE_URL}" ]
  [ -n "${JIRA_USERNAME}" ]
  [ -n "${JIRA_PASSWORD}" ]
}

@test "JIRA environment variables MUST be sufficient for REST API access." {
  run http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} \
    ${JIRA_BASE_URL}/rest/api/2/user?username=${JIRA_USERNAME}
  [ "$status" -eq 0 ]
  [ $(expr "$output" : '.*\"key\":\"admin\"') -ne 0 ]
}

@test "Confluence environment variables MUST be available." {
  [ -n "${CONF_SITENAME}" ]
  [ -n "${CONF_BASE_URL}" ]
  [ -n "${CONF_USERNAME}" ]
  [ -n "${CONF_PASSWORD}" ]
}

@test "Confluence environment variables MUST be sufficient for REST API access." {
  run http -a ${CONF_USERNAME}:${CONF_PASSWORD} \
    ${CONF_BASE_URL}/rest/api/user?username=${CONF_USERNAME}
  [ "$status" -eq 0 ]
  [ $(expr "$output" : '.*\"username\":\"admin\"') -ne 0 ]
}
