#!/bin/bash

#################################################
#										   		#
#	Sets the environment variables needed		#
#	for the exercises to make the calls 		#
#	easier and more consistent. 				#
#										   		#
#										   		#
#	Run this by calling . ./setEnv.sh			#
#	The leading '.' makes the export reflect	#
#	in the current shell 						#
#												#
#################################################

# Username used will be admin so no need to set
# ATLDEV_INSTANCE_NAME is ${ATLDEV_INSTANCE_NAME}.atlassian.net
ATLDEV_PASSWORD=
ATLDEV_INSTANCE_NAME=

# Will be used for JIRA endpoints .
export JIRA_SITENAME=${ATLDEV_INSTANCE_NAME}
export JIRA_USERNAME=admin
export JIRA_PASSWORD=${ATLDEV_PASSWORD}
export JIRA_BASE_URL=https://${JIRA_SITENAME}.atlassian.net

# Will be used for Confluence endpoints.
export CONF_SITENAME=${JIRA_SITENAME}
export CONF_USERNAME=${JIRA_USERNAME}
export CONF_PASSWORD=${JIRA_PASSWORD}
export CONF_BASE_URL=https://${CONF_SITENAME}.atlassian.net/wiki
