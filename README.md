You can access the keynote [here](https://acalantog.bitbucket.io/Summit2017)

# Setup of Tools:

* [Create JIRA and Confluence cloud dev instance](http://go.atlassian.com/cloud-dev)
* [Create cloud IDE instance](http://codeanywhere.com)


### Cloning the Repository

``` bash
git clone https://iragudo@bitbucket.org/iragudo/summitrest.git
```


### Setting up the environment variables:

 1. Open from CodeAnywhere file setEnv.sh
 2. Enter atlassian instance name and user password
 3. Save
 4. From the container terminal, go to scripts directory: `cd ~/workspace/summitrest/scripts/`
 5. Enter command to update: `source setEnv.sh`


### Install HTTPie: 

 1. From the container terminal, inside the directory: `~/workspace/summitrest/scripts/`
 2. Enter command to install: `sh setupHttpie.sh`


### Install BATS:
 1. From the container terminal, inside the directory: `~/workspace/summitrest/scripts/`
 2. Enter command to install: `sh setupBats.sh`

### Run BATS:
 1. From the container terminal, inside the directory: `~/workspace/summitrest/scripts/`
 2. Enter command to install: `bats testSetup.bats`
---


# JIRA

* [JIRA cloud REST API doc site](https://docs.atlassian.com/jira/REST/cloud/)
* [JIRA search using JQL](https://confluence.atlassian.com/jirasoftwarecloud/advanced-searching-764478330.html)

### Endpoint Structure
``` bash
https://instance-name/rest/{api-name}/{api-version}/{resource-name}
```

### REST call via HTTPie
``` bash
http GET ${JIRA_BASE_URL}/rest/api/latest/serverInfo
```

### REST call via curl
``` bash
curl -i -X GET ${JIRA_BASE_URL}/rest/api/2/serverInfo
```

### With Basic Auth
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} GET ${JIRA_BASE_URL}/rest/api/latest/myself
```
``` bash
curl -i -u ${JIRA_USERNAME}:${JIRA_PASSWORD} -X GET ${JIRA_BASE_URL}/rest/api/latest/myself
```

### With Parameters
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} GET ${JIRA_BASE_URL}/rest/api/latest/user?username=admin
```


### With Request Body
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} POST ${JIRA_BASE_URL}/rest/api/latest/project key=BOOT name="BOOTCAMP Project" lead=admin projectTypeKey=software assigneeType=UNASSIGNED
```


### Create Issue
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} POST ${JIRA_BASE_URL}/rest/api/latest/issue fields:='
{
  "project": {
    "key": "BOOT"
  },
  "summary": "[Load Balancer] OutOfMemoryError",
  "description": "The details of the incident derived from a file.",
  "issuetype": {
    "name": "Bug"
  }
}'
```


### Create Meta
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} GET ${JIRA_BASE_URL}/rest/api/latest/issue/createmeta 
```


### Get Issue
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} GET ${JIRA_BASE_URL}/rest/api/latest/issue/{issueKey}
```


### Edit Issue
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} PUT ${JIRA_BASE_URL}/rest/api/latest/issue/{issueKey} fields:='
{
  "summary": "[myFilter] REST Ye Merry Gentlemen."
}'
```


### Delete Issue
``` bash
http -a ${JIRA_USERNAME}:${JIRA_PASSWORD} DELETE ${JIRA_BASE_URL}/rest/api/latest/issue/{issueKey}
```
---


# Confluence

* [Confluence cloud REST API site](https://docs.atlassian.com/confluence/REST/latest/)
* [Confluence REST API Examples](https://developer.atlassian.com/cloud/confluence/rest-api-examples/)
* [Confluence storage format site](https://confluence.atlassian.com/confcloud/confluence-storage-format-724765084.html)
* [Escape Complex HTML](https://www.freeformatter.com/json-escape.html)
* [Shell Checker](https://www.shellcheck.net/)

### Endpoint Structure
`https://instance-name.atlassian.net/wiki/rest/api/resource-name`

#### Space Creation
``` bash
http -a ${CONF_USERNAME}:${CONF_PASSWORD} POST ${CONF_BASE_URL}/rest/api/space key=TST name="REST Space" description:='
{
  "plain": {
    "representation": "plain",
    "embeddedContent": []
  }
}'
```


#### Page Creation
```bash
http -a ${CONF_USERNAME}:${CONF_PASSWORD} POST ${CONF_BASE_URL}/rest/api/content space:='{"key": "TST"}' ancestors:='[{"type":"page","id":"491522"}]' type="page" title="New page via REST" body:='
{
  "storage": {
    "value": "Simple Page",
    "representation": "storage"
  }
}'
```


---


# OAuth